<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Hash;
use Yajra\DataTables\Facades\DataTables;

class User extends Authenticatable
{
    use Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function scopeHasRoles(\Illuminate\Database\Eloquent\Builder $query, $roles)
    {
        return $this->scopeRole($query, $roles);
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public static function registerUser($request)
    {
        $user = User::create([
            'name' => request('name'),
            'username' => request('username'),
            'password' => request('password'),
            'rel_code' => request('rel_code')
        ]);
        return true;
    }

    public static function usersDatatable($request){

        $users = User::query()->with('roles')
            ->select('users.*');
        return DataTables::eloquent($users)
            ->addColumn('action', function ($users) {
                //render button
                $roles = $users['roles'][0]['name'];
                if($roles=='administrator'){
                    $button = '';
                }else{
                    $button = '<a href="'.route('users.show',$users->id).'" class="btn btn-success btn-circle">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                        <a href="'.route('users.edit',$users->id).'" class="btn btn-info btn-circle">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-circle" data-id="'.$users->id.'"   data-toggle="modal" data-target="#deleteModal">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>';
                }


                return $button;
            })
            ->addColumn('roles.name', function ($users) {
                $html = '';

                foreach($users->roles()->pluck('name') as $role){
                    $html.= ' <span class="badge badge-primary">'.$role.'</span>';
                }
                return  $html;
            })

            ->editColumn('created_at', function ($users) {
                $created_at = date('M d, Y', strtotime($users->created_at));
                return $created_at;
            })
            ->filterColumn('roles', function ($query, $keyword) {
                $query ->selectraw("select name from roles where name = ?", ["%$keyword%"]);
            })
            ->rawColumns(['roles.name','action'])
            ->make(true);
    }

    public static function userStore($request){

        //create user
        $user = User::create($request->all());

        //assigning roles to user
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->assignRole($roles);

        return true;
    }

    public static function userUpdate($request,$id){

        //get user data
        $user = User::findOrFail($id);

        //update user
        $user->update($request->all());

        //reSync roles to image
        $roles = $request->input('roles') ? $request->input('roles') : [];
        $user->syncRoles($roles);

        return true;
    }

    public static function profileUpdate($request){

        //get user data
        $user = auth()->user();

        //update user
        $user->update($request->all());

//        //reSync roles to image
//        $roles = $request->input('roles') ? $request->input('roles') : [];
//        $user->syncRoles($roles);

        return true;
    }
}
