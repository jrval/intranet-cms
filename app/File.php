<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    protected $table = 'files';
    public $timestamps = true;
    protected $fillable = array('path', 'post_id', 'ext','file_origname','filesize');

    public function post()
    {
        return $this->belongsTo('App\Posts', 'post_id');
    }

}