<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name' => 'required',
            'username' => 'required|unique:users,username|regex:/(^[A-Za-z0-9-_]+$)+/',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
            'roles' => 'required',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'username' => 'username',
            'password' => 'password',
            'name' => 'name',
            'roles' => 'roles',
        ];
    }
}
