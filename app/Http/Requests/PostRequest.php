<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'post_title' => 'required',
                    'category_id' => 'required',
                    'image_path' => 'nullable|image|mimes:jpeg,jpg,png|max:2000',
                    'post_content' => 'nullable',
                    'path' => 'file|max:10000',
                    'path.*' => 'mimes:doc,pdf,docx,zip,xls,ppt'
                ];
                break;

            default:
                $rules = [
                    'post_title' => 'required',
                    'category_id' => 'required',
                    'image_path' => 'nullable|image|mimes:jpeg,jpg,png|max:2000',
                    'post_content' => 'nullable',
                    'path' => 'file|max:10000',
                    'path.*' => 'mimes:doc,pdf,docx,zip,xls,ppt'
                ];
                break;
            }

            return $rules;
        }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'post_title' => 'post title',
            'category_id' => 'category',
            'image_path' => 'image',
            'post_content' => 'content',
            'path.*' => 'files'
        ];

    }
}
