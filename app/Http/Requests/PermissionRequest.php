<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'name' => 'required|string|max:20|unique:permissions,name,'.$this->permission.',|regex:/(^[A-Za-z0-9-_]+$)+/',
                ];
                break;

            default:
                $rules = [
                    'name' => 'required|string|max:20|unique:permissions,name,|regex:/(^[A-Za-z0-9-_]+$)+/',
                ];
                break;

        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'name' => 'permission name',
        ];
    }
}
