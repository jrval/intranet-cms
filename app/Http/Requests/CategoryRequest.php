<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        switch ($this->method()) {
            case 'PUT':
                $rules = [
                    'category_name' => 'required|string|max:20|unique:categories,category_name,'.$this->category,
                ];
                break;

            default:
                $rules = [
                    'category_name' => 'required|string|max:20|unique:categories,category_name',
                ];
                break;

        }
        return $rules;
    }

    public function attributes()
    {
        return [
            'category_name' => 'category name',
        ];
    }
}
