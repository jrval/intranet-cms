<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->user==null){
            $user=auth()->user()->id;
        }else{
            $user=$this->user;
        }

        $rules = [
            'name' => 'required',
            'username' => 'required|unique:users,username,'.$user.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'password' => 'nullable',
            'password_confirmation' => 'required_with:password|same:password',
            'roles' => 'sometimes',
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'username' => 'username',
            'password' => 'password',
            'name' => 'name',
            'roles' => 'roles',
        ];
    }

}
