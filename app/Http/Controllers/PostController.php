<?php
/**
 * Copyright (c) 2019. JR Valencia (Developer).
 * Email: valencia_jr08@live.com
 * Redistribution and use in source and binary forms are permitted provided that the above copyright notice and this paragraph are duplicated in all such forms and that any documentation, advertising materials, and other materials related to such distribution and use acknowledge that the software was developed by me.
 *
 * I may not be used to endorse or promote products derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED "AS IN" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE .
 */

namespace App\Http\Controllers;

use App\Category;
use App\File as files;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.post.index');
    }

    public function postDatatable(Request $request){

        $post = Post::postDatatable($request);

        return $post;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //roles collection
        $categories = Category::OrderBy('category_name','asc')->pluck('category_name', 'id');

        //return json to view
        return view('pages.post.create')->with(compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $post = Post::storePost($request);

        session()->flash('message', 'Post successfully created.');
        session()->flash('alert-class', 'alert-success');

        return redirect(route('posts.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('pages.post.show')->with(compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //categories collection
        $categories = Category::OrderBy('category_name','asc')->pluck('category_name', 'id');
        //get post
        $post = Post::with('file')->findOrFail($id);
        //return json to view
        return view('pages.post.edit')->with(compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        Post::updatePost($request,$id);

        session()->flash('message', 'Post successfully updated.');
        session()->flash('alert-class', 'alert-info');
        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $oldImgFile =  $post->image_path;

        File::delete(public_path().'/images/'.$oldImgFile);

        $files = files::where('post_id',$id)->get();
        foreach($files as $file){
            $file_path = $file->path;
            if($file){
                $destroy = files::destroy($file->id);
                File::delete(public_path().'/files/'.$file_path);
            }
        }
        $post->delete();

        session()->flash('message', 'Post successfully deleted.');
        session()->flash('alert-class', 'alert-danger');
        return redirect(route('posts.index'));
    }
}
