<?php
/**
 * Copyright (c) 2019. JR Valencia (Developer).
 * Email: valencia_jr08@live.com
 * Redistribution and use in source and binary forms are permitted provided that the above copyright notice and this paragraph are duplicated in all such forms and that any documentation, advertising materials, and other materials related to such distribution and use acknowledge that the software was developed by me.
 *
 * I may not be used to endorse or promote products derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED "AS IN" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE .
 */

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.user.index');
    }

    public function usersDatatable(Request $request){
        $users = User::usersDatatable($request);
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //roles collection
        $roles = Role::where('name','!=','administrator')->pluck('name', 'name');

        //return json to view
        return view('pages.user.create')->with(compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        User::userStore($request);
        session()->flash('message', 'User successfully created.');
        session()->flash('alert-class', 'alert-success');
        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('pages.user.show')->with(compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //roles collection
        $roles = Role::where('name','!=','administrator')->pluck('name', 'name');
        //get user data
        $user = User::findOrFail($id);

        //return json to view
        return view('pages.user.edit')->with(compact('user','roles'));
    }


    public function profile()
    {
        //roles collection
        $roles = Role::where('name','=','administrator')->pluck('name', 'name');
        //get user data
        $user = auth()->user();

        //return json to view
        return view('pages.user.profile_edit')->with(compact('user','roles'));
    }

    public function profileUpdate(UserUpdateRequest $request)
    {
        //update user
        User::profileUpdate($request);
        session()->flash('message', 'Profile successfully updated.');
        session()->flash('alert-class', 'alert-info');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        //update user
        User::userUpdate($request,$id);

        session()->flash('message', 'User successfully updated.');
        session()->flash('alert-class', 'alert-info');
        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find user
        $user = User::findOrFail($id);
        //delete user
        $user->delete();

        session()->flash('message', 'User successfully deleted.');
        session()->flash('alert-class', 'alert-danger');
        return redirect(route('users.index'));
    }
}
