<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Yajra\DataTables\Facades\DataTables;

use Image;

use File;

use Illuminate\Support\Facades\URL;

use Illuminate\Support\Facades\Input;

use Carbon\Carbon;

use App\File as files;

class Post extends Model
{

    protected $table = 'posts';
    public $timestamps = true;
    protected $fillable = array('category_id', 'post_title', 'image_path', 'post_content');

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function file()
    {
        return $this->hasMany('App\File');
    }

    public static function storePost($request){


        $post = new Post();
        $post->post_title = $request->post_title;
        $post->post_content = $request->post_content;
        $post->category_id = $request->category_id;

        if($request->image_path_img_base_64_add != null){

            $destinationPath = public_path().'/images/';

            $img_file = $request->input('image_path_img_base_64_add');

            $file = str_replace('data:image/png;base64,', '',$img_file );

            $img = str_replace(' ', '+', $file);

            $data = base64_decode($img);

            $filename = uniqid().'_cropped_image_'.time().'.png';

            $file = $destinationPath . $filename;

            Image::make($data)->resize(1020, 600, function ($constraint) {
                $constraint->aspectRatio();
            })->save($file);

            $post->image_path = $filename;

            //create thumbnail
            $thumbPath = public_path().'/images/thumbnails';
            Image::make($data)->resize(180, 180)->save($thumbPath . '/' . $filename);
        }

        $post->save();

        if($request->hasfile('path'))
        {

            foreach($request->file('path') as $file)
            {
                $filesize = $file->getSize();
                $extension = $file->getClientOriginalExtension();
                $orig_filename=$file->getClientOriginalName();
                $name = pathinfo($orig_filename, PATHINFO_FILENAME);
                $filename = $name.'_'.uniqid().'_'.time().'.'.$extension;
                $file->move(public_path().'/files/', $filename);
                $file= new files();

                $file->path = $filename;
                $file->file_origname = $name;
                $file->filesize = $filesize;
                $file->post_id = $post->id;
                $file->ext = $extension;
                $file->save();
            }
        }

        return true;

    }


    public static function updatePost($request,$id){

        $post = Post::find($id);
        $post->post_title = $request->post_title;
        $post->post_content = $request->post_content;
        $post->category_id = $request->category_id;

        if($request->image_path_img_base_64_add != null){

            $destinationPath = public_path().'/images/';

            $img_file = $request->input('image_path_img_base_64_add');

            $file = str_replace('data:image/png;base64,', '',$img_file );

            $img = str_replace(' ', '+', $file);

            $data = base64_decode($img);

            $filename = uniqid().'_cropped_image_'.time().'.png';

            $file = $destinationPath . $filename;

            Image::make($data)->resize(1020, 600, function ($constraint) {
                $constraint->aspectRatio();
            })->save($file);

            $oldImgFile =   $post->image_path;

            $post->image_path = $filename;

            //create thumbnail
            $thumbPath = public_path().'/images/thumbnails';
            Image::make($data)->resize(180, 180)->save($thumbPath . '/' . $filename);

            File::delete(public_path('/images/'.$oldImgFile));
            File::delete(public_path('/images/thumbnails/'.$oldImgFile));
        }

        $post->save();

        if($request->has('file_remove')){

            $deleted = $request->file_remove;

            foreach($deleted as $del){
                $file = files::find($del);
                $file_path = $file->path;
                if($file){
                    $destroy = files::destroy($del);
                    File::delete(public_path().'/files/'.$file_path);
                }

            }
        }


        if($request->hasfile('path'))
        {

            foreach($request->file('path') as $file)
            {
                $filesize = $file->getSize();
                $extension = $file->getClientOriginalExtension();
                $orig_filename=$file->getClientOriginalName();
                $name = pathinfo($orig_filename, PATHINFO_FILENAME);
                $filename = $name.'_'.uniqid().'_'.time().'.'.$extension;
                $file->move(public_path().'/files/', $filename);

                $file= new files();
                $file->path = $filename;
                $file->post_id = $post->id;
                $file->file_origname = $name;
                $file->filesize = $filesize;
                $file->post_id = $post->id;
                $file->ext = $extension;
                $file->save();
            }
        }

        return true;

    }
    public static function postDatatable($request){

        $post = Post::query()->with('category')
            ->select('posts.*');
        
        return DataTables::eloquent($post)

            ->addColumn('category', function ($post) {

//                $color = ['badge-primary','badge-success','badge-warning','badge-danger','badge-info'];
//                $html= '<span class="badge '.$color[$post->category->id].'">'.$post->category->category_name.'</span>';

                $cat = $post->category->category_name;
                return  $cat;
            })

            ->addColumn('action', function ($post) {
                //render button

                $button = '<a href="'.route('posts.show',$post->id).'" class="btn btn-success btn-circle">
                                    <i class="fas fa-eye"></i>
                                </a>
                                <a href="'.route('posts.edit',$post->id).'" class="btn btn-info btn-circle">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="#" class="btn btn-danger btn-circle" data-id="'.$post->id.'"   data-toggle="modal" data-target="#deleteModal">
                                    <i class="fas fa-trash-alt"></i>
                                </a>';

                return $button;
            })

            ->editColumn('image_path', function ($post) {
                $html = '<a href="'.asset('images/'.$post->image_path).'" data-fancybox data-caption="'.$post->post_title.'">
                            <img src="'.asset('images/thumbnails/'.$post->image_path).'" alt=""  width="80"/>
                        </a>';

                return $html;
            })

            ->editColumn('created_at', function ($post) {
                $created_at = date('M d, Y', strtotime($post->created_at));
                return $created_at;
            })

            ->addColumn('timestamps', function ($post) {
                $timestamps = $post->created_at->timestamp;
                return $timestamps;
            })


            ->filterColumn('category', function ($query, $keyword) {
                $query ->selectraw("select category_name from categories where category_name = ?", ["%$keyword%"]);
            })
            ->rawColumns(['category','action','image_path'])
            ->make(true);
    }

}