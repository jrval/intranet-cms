<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilesTable extends Migration {

	public function up()
	{
		Schema::create('files', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->char('path');
			$table->integer('post_id')->unsigned();
			$table->string('ext')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('files');
	}
}