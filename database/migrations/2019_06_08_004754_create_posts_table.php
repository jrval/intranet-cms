<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('category_id')->unsigned();
			$table->string('post_title');
			$table->char('image_path')->nullable();
			$table->longText('post_content')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('posts');
	}
}