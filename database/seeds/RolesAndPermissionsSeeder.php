<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;


class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => 'manage_users','description'=>'permission to manage a users']);
        Permission::create(['name' => 'manage_content','description'=>'permission to manage intranet content']);

        // create roles and assign created permissions
        /*
                $role = Role::create(['name' => 'writer']);
                $role->givePermissionTo('edit articles');

                $role = Role::create(['name' => 'moderator']);
                $role->givePermissionTo(['publish articles', 'unpublish articles']);*/

        //create Role
        $role = Role::create(['name' => 'administrator','description'=>'administrator']);
        $role->givePermissionTo('manage_users');


        //Create admin
        $user = User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'password' => 'password',
        ]);

        $user->assignRole('administrator');
    }
}
