@extends('layouts.app')
@section('title',$post->post_title)
@section('content')
    <style>
        .cropit-preview {
            background: url({{asset('images/'.$post->image_path)}});
            background-color: #f8f8f8;
            background-size: cover;
            border: 1px solid #ccc;
            border-radius: 3px;
            margin: auto;
            width: 1020px;
            height: 600px;
        }
    </style>
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Posts | {{$post->post_title}}</h1>
                </div>
                <div class="col-lg-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <a href="{{route('posts.edit',$post->id)}}" class="btn btn-info btn-icon-split">
                    <span class="icon text-white-50">
                     <i class="fas fa-edit"></i>
                    </span>
                                <span class="text">Edit Post</span>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Title:</h3>
                                </div>
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    <h3>{{$post->post_title}}</h3>
                                </div>
                            </div>
                            @if($post->image_path!=null)
                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Image:</h3>
                                </div>
                                <div class="col-sm-12 mb-3 mb-sm-0">
                                    <img class="img-fluid" src="{{asset('images/'.$post->image_path)}}">
                                </div>

                            </div>
                            @endif
                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Category:</h3>
                                </div>
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    <p class="text-info">{{$post->category->category_name}}</p>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Attachments:</h3>
                                </div>
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    @forelse($post->file as $f)
                                        @if($f->ext =='pdf')
                                            @php
                                                $icon = 'fa-file-pdf'
                                            @endphp
                                        @elseif($f->ext=='zip')
                                            @php
                                                $icon = 'fa-file-archive'
                                            @endphp
                                        @elseif($f->ext=='doc' || $f->ext=='docx')
                                            @php
                                                $icon = 'fa-file-word'
                                            @endphp
                                        @elseif($f->ext=='xls')
                                            @php
                                                $icon = 'fa-file-excel'
                                            @endphp
                                        @elseif($f->ext=='ppt')
                                            @php
                                                $icon = 'fa-file-powerpoint'
                                            @endphp
                                        @endif

                                        <a href="{{asset('/files/'.$f->path)}}" download="{{$f->file_origname}}"><i class="fas {{$icon}}"></i> {{$f->file_origname}}.{{$f->ext}} </a>
                                        ({{\App\Helpers\Hlpr::bytesToHuman($f->filesize)}})
                                        <br>
                                    @empty
                                        <p class="text-danger">no current files</p>
                                    @endforelse
                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Content:</h3>
                                </div>
                                @if($post->post_content != null)
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    <p class="text-info">{{$post->post_content}}</p>
                                </div>
                                @else
                                    <div class="col-sm-9 mb-3 mb-sm-0">
                                        <p class="text-info">No Content</p>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Date created:</h3>
                                </div>
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    <h3>{{\Carbon\Carbon::parse($post->created_at)->format('M d, Y')}}</h3>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    @include('pages.post._script')
@endsection