
<script>
    let post_id = '{{isset($post) ? $post->id : ""}}';
    /*render delete modal*/
    $('#deleteModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var post_id = button.data('id');// Extract info from data-* attributes

        var modal = $(this);

        var url = '{{ route("posts.destroy", ":post_id") }}';
        url = url.replace(':post_id', post_id);

        modal.find('#delete-form').attr('action', url);
        modal.find('.modal-title').html('DELETE POST');
        modal.find('.modal-body').html('Are you sure you want to delete this post?');
    });



    {{--DATATABLES--}}
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#posts-datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax:{
                "url": '{!! route("post.dataTable") !!}',
                "type": "POST",
            },
            columns: [
                { data: 'post_title', name: 'post_title' },
                { data: 'image_path', name: 'image_path',searchable:false,orderable:false},
                { data: 'category', name: 'category.category_name' },
                { data: 'created_at', name: 'created_at'},
                { data: 'timestamps', name: 'timestamps'},
                { data: 'action', name: 'action',searchable:false,orderable:false},

            ],
            'columnDefs': [
                { 'orderData':[4], 'targets': [3] },
                {
                    'targets': [4],
                    'visible': false,
                    'searchable': false
                },
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 4, targets: -2 }
            ],
            "order": [[ 4, "desc" ]], //or asc
        });

        $(".btn-add").click(function(){
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-remove",function(){
            $(this).parents(".control-group").remove();
        });


    });

    @isset($post->image_path)
        var defaultImg = '{{asset('images/'.$post->image_path)}}';
    @else
    var defaultImg = '';
            @endisset
    var imgExported;

    $('#image-cropper').cropit({
        'allowDragNDrop': false,
        imageState: {
            src: defaultImg,
        },
        'minZoom': 'fit',
        'maxZoom': '1.5',
        initialZoom: 'min',
        'width':'1020',
        'height':'600',
        'smallImage':'allow',
        'export': {
            type: 'image/jpeg/jpg/png',
        },
        onFileReaderError: function(){

            // $('#image-cropper').cropit('imageSrc',defaultImg);
            $("#image_path_img_base_64_add").val('');
            alert('please select image only');
        },
        onImageLoaded: function(){
            $(".cropit-preview").css({"background": "white"});
        }
    });

    $('.select-image-btn').click(function() {
        $('.cropit-image-input').click();
    });

    $('.rotate-cw-btn').click(function() {
        $('#image-cropper').cropit('rotateCW');
    });
    $('.rotate-ccw-btn ').click(function() {
        $('#image-cropper').cropit('rotateCCW');
    });

    $('#btn-submit').click(function () {
        imgExported = $('#image-cropper').cropit('export');
        $("#image_path_img_base_64_add").val(imgExported);

    });


</script>