@extends('layouts.app')
@section('title','Posts')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Posts</h1>
                    @if (session('message'))
                        <div class="alert {{session('alert-class')  }}">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                <div class="col-lg-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <a href="{{route('posts.create')}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                     <i class="fas fa-plus"></i>
                    </span>
                                <span class="text">Create Posts</span>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable" width="100%" cellspacing="0" style="font-size: 0.9em" id="posts-datatable">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Date Created</th>
                                        <th>Time Stamps</th>
                                        <th>Options</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Category</th>
                                        <th>Date Created</th>
                                        <th>Time Stamps</th>
                                        <th>Options</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    @include('pages.post._script')
@endsection