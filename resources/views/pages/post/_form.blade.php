@if(isset($post))

    {{ Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'put','id'=>'form','files'=>'true','class'=>'user']) }}

@else
    {{ Form::open(['route' => 'posts.store','id'=>'form','files'=>'true','class'=>'user']) }}
@endif

<style>
    .cropit-preview {
        background: url({{asset('images/no-image.jpg')}});
        background-color: #f8f8f8;
        background-size: cover;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin: auto;
        width: 1020px;
        height: 600px;
    }

    .cropit-preview-image-container {
        cursor: move;
    }

    input.cropit-image-input {
        visibility: hidden;
    }
    .hide{
        display: none;
    }
</style>

{{--image--}}
<div class="form-group row">
    <div class="col-sm-12 mb-3 mb-sm-0 {{ $errors->has('image_path') ? ' has-error' : '' }}">
        <label for="name" class="control-label" aria-describedby="name">Image (allowed formats: .jpeg, .jpg, .png | max upload size: 2MB)</label>
        <div id="image-cropper" class="form-group">
            <!-- This is where the preview image is displayed -->
            <div class="cropit-preview"></div>
            <!-- This range input controls zoom -->
            <!-- You can add additional elements here, e.g. the image icons -->
            <br>
            <input type="range" class="cropit-image-zoom-input " />
            <!-- This is where user selects new image -->
            <input type="file" name="image_path" id="image_path" class="cropit-image-input form-control" accept=".jpeg,.jpg,.png" />
            <!-- The cropit- classes above are needed
                 so cropit can identify these elements -->
            <button class="btn btn-success waves-effect select-image-btn" type="button"><i class="fas fa-images"></i> Select image</button>
            <button class="btn btn-warning rotate-ccw-btn" type="button"><i class="fas fa-undo-alt"></i></button>
            <button class="btn btn-info rotate-cw-btn" type="button"><i class="fas fa-redo-alt"></i></button>

            <input type="hidden" id="image_path_img_base_64_add" name="image_path_img_base_64_add" value="">
        </div>

        @if ($errors->has('image_path'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('image_path') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--title--}}
<div class="form-group row required">
    <div class="col-sm-12 mb-3 mb-sm-0 {{ $errors->has('post_title') ? ' has-error' : '' }}">
        <label for="name" class="control-label" aria-describedby="name">Title</label>
        {{Form::text('post_title', old('post_title'),['class' => 'form-control form-control-user','id'=>'name','placeholder'=>'Post Title']) }}
        @if ($errors->has('post_title'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('post_title') }}</strong>
            </span>
        @endif
    </div>
</div>
{{--category--}}
<div class="form-group row required">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('category_id') ? ' has-error' : '' }}">
        <label for="roles" class="control-label" aria-describedby="roles">Category</label>
        @if(isset($user))
            {!! Form::select('category_id', $categories, old('category_id') ? old('category_id') : $post->category()->pluck('category_name', 'id'), ['class' => 'form-control', 'id'=>'select-category',]) !!}
        @else
            {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control','id'=>'select-category',]) !!}
        @endif

        @if ($errors->has('category_id'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('category_id') }}</strong>
             </span>
        @endif
    </div>
</div>


{{--content--}}
<div class="form-group row">
    <div class="col-sm-12 mb-3 mb-sm-0 {{ $errors->has('post_content') ? ' has-error' : '' }}">
        <label for="name" class="control-label" aria-describedby="name">Content</label>
        {{Form::textarea('post_content', old('post_content'),['class' => 'form-control','id'=>'name','placeholder'=>'Post Content']) }}

        @if ($errors->has('post_content'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('post_content') }}</strong>
            </span>
        @endif
    </div>
</div>


@if(isset($post))


    <div class="form-group row">
        <div class="col-sm-12 mb-3 mb-sm-0">
            <label for="Current File" class="control-label" aria-describedby="Current File">Current Attachments</label>
            <br>
            @forelse($post->file as $f)
                @if($f->ext =='pdf')
                    @php
                    $icon = 'fa-file-pdf'
                    @endphp
                @elseif($f->ext=='zip')
                    @php
                        $icon = 'fa-file-archive'
                    @endphp
                @elseif($f->ext=='doc' || $f->ext=='docx')
                    @php
                        $icon = 'fa-file-word'
                    @endphp
                @elseif($f->ext=='xls')
                    @php
                        $icon = 'fa-file-excel'
                    @endphp
                @elseif($f->ext=='ppt')
                    @php
                        $icon = 'fa-file-powerpoint'
                    @endphp
                @endif

                    <a href="{{asset('/files/'.$f->path)}}" download="{{$f->file_origname}}"><i class="fas {{$icon}}"></i> {{$f->file_origname}}.{{$f->ext}} </a>
                    ({{\App\Helpers\Hlpr::bytesToHuman($f->filesize)}})
                        <p class="text-danger d-inline">remove?  {!! Form::checkbox('file_remove[]', $f->id) !!}</p>
                    <br>
            @empty
                <p class="text-danger">no current files</p>
            @endforelse

        </div>
    </div>
@endif

<div class="form-group row">
    <div class="col-sm-12 mb-3 mb-sm-0 {{ $errors->has('path.*') ? ' has-error' : '' }}">
        <label for="name" class="control-label" aria-describedby="name">Add Attachments (allowed formats: .pdf, .doc, .docx, .xls, .ppt, .zip, .xls, .ppt | max file size: 10MB)</label>
        <div class="input-group control-group increment" >
            <input type="file" name="path[]" class="form-control" accept=".pdf,.doc,.docx,.xls,.ppt,.zip,.xls,.ppt">
{{--            {!! Form::file('path[]', old('path[]'),['class' => 'form-control','placeholder'=>'upload file','accept'=>'.pdf,.doc,.docx,.xls,.ppt,.zip,.xls,.ppt']) !!}--}}
            <div class="input-group-btn">
                <button class="btn btn-success btn-add" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
            </div>
        </div>
        <div class="clone hide">
            <div class="control-group input-group" style="margin-top:10px">
{{--                <input type="file" name="path[]" class="form-control">--}}
{{--                {!! Form::file('path[]', old('path[]'),['class' => 'form-control','placeholder'=>'upload file','accept'=>'.pdf,.doc,.docx,.xls,.ppt,.zip,.xls,.ppt']) !!}--}}
                <input type="file" name="path[]" class="form-control" accept=".pdf,.doc,.docx,.xls,.ppt,.zip,.xls,.ppt">
                <div class="input-group-btn">
                    <button class="btn btn-danger btn-remove" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                </div>
            </div>
        </div>
        @if ($errors->has('path.*'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('path.*') }}</strong>
            </span>
        @endif
    </div>
</div>



@if(isset($post))
    <button type="submit" id="btn-submit" class="btn btn-primary btn-user btn-block">
        Update
    </button>
@else
    <button type="submit" id="btn-submit" class="btn btn-primary btn-user btn-block">
        Create
    </button>
@endif

<hr>

{{ Form::close() }}
