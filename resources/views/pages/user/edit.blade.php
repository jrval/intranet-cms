@extends('layouts.app')
@section('title','Edit-user')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Edit User</h1>
                    @if (session('message'))
                        <div class="alert {{session('alert-class')  }}">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                @include('pages.user._form')
            </div>
        </div>
    </div>
@endsection