@if(isset($user))

    @if(Request::segment(1)=='profile')
        {{ Form::model($user, ['route' => ['profile.update'], 'method' => 'put','id'=>'form','files'=>'true','class'=>'user']) }}
    @else
        {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'put','id'=>'form','files'=>'true','class'=>'user']) }}
    @endif

@else
    {{ Form::open(['route' => 'users.store','id'=>'form','files'=>'true','class'=>'user']) }}
@endif
<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name" class="control-label" aria-describedby="name">Name</label>
        {{Form::text('name', old('name'),['class' => 'form-control form-control-user','id'=>'name','placeholder'=>'Name']) }}
        @if ($errors->has('name'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('username') ? ' has-error' : '' }}">
        <label for="username" class="control-label" aria-describedby="username">Username</label>
        {{Form::text('username', old('username'),['class' => 'form-control form-control-user','id'=>'username','placeholder'=>'Username']) }}
        @if ($errors->has('username'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('username') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password" class="control-label" aria-describedby="password">Password</label>

        @if(isset($user))
        {{Form::password('password',['class' => 'form-control form-control-user','id'=>'password','placeholder'=>'Password']) }}
            @if ($errors->has('password'))
                <span class="help-block" style="color: #a94442;">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
            <div class="help-info">Leave blank if you don't want to change password</div>
        @else
            <small class="form-text  text-danger">
                The default password is "password".
            </small>
            <input type="hidden" name="password" value="password">
        @endif
    </div>
    @if(isset($user))
        <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password" class="control-label" aria-describedby="password">Password Confirmation</label>
            {{Form::password('password_confirmation',['class' => 'form-control form-control-user','id'=>'password_confirmation','placeholder'=>'Password Confirmation']) }}
            @if ($errors->has('password_confirmation'))
                <span class="help-block" style="color: #a94442;">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
        </div>
    @else
        <input type="hidden" name="password_confirmation" value="password">
    @endif
</div>
@if(auth()->user()->id != $user->id)
<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('$roles') ? ' has-error' : '' }}">
        <label for="roles" class="control-label" aria-describedby="roles">Roles</label>
        @if(isset($user))
            {!! Form::select('roles', $roles, old('$roles') ? old('$roles') : $user->roles()->pluck('name', 'name'), ['class' => 'form-control', 'id'=>'select-permission',]) !!}
        @else
            {!! Form::select('roles', $roles, old('roles'), ['class' => 'form-control','id'=>'select-permission',]) !!}
        @endif

        @if ($errors->has('roles'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('roles') }}</strong>
             </span>
        @endif
    </div>
</div>
@endif
@if(isset($user))
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Update
    </button>
@else
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Create
    </button>
@endif

<hr>

{{ Form::close() }}
