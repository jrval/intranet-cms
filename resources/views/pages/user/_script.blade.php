
<script>
    let user_id = '{{isset($user) ? $user->id : ""}}';
    /*render delete modal*/
    $('#deleteModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var user_id = button.data('id');// Extract info from data-* attributes

        var modal = $(this);

        var url = '{{ route("users.destroy", ":user_id") }}';
        url = url.replace(':user_id', user_id);

        modal.find('#delete-form').attr('action', url);
        modal.find('.modal-title').html('DELETE USER');
        modal.find('.modal-body').html('Are you sure you want to delete this user?');
    });
    {{--DATATABLES--}}
    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#users-datatable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,

            ajax:{
                "url": '{!! route("users.dataTable") !!}',
                "type": "POST",
            },
            columns: [
                { data: 'name', name: 'name' },
                { data: 'username', name: 'username' },
                { data: 'roles.name', name: 'roles.name' },
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action',searchable:false,orderable:false},

            ],
            'columnDefs': [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 4, targets: -2 }
            ],
            "order": [[ 2, "asc" ]], //or asc
        });
    });

</script>