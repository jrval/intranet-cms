<script>
    /*render delete modal*/
    $('#deleteModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var role_id = button.data('id');// Extract info from data-* attributes

        var modal = $(this);

        var url = '{{ route("roles.destroy", ":role_id") }}';
        url = url.replace(':role_id', role_id);

        modal.find('#delete-form').attr('action', url);
        modal.find('.modal-title').html('DELETE ROLE');
        modal.find('.modal-body').html('Are you sure you want to delete this role?');
    });
</script>