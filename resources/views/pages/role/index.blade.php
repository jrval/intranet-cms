@extends('layouts.app')
@section('title','Roles')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Roles</h1>
                    @if (session('message'))
                        <div class="alert {{session('alert-class')  }}">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                <div class="col-lg-12">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <a href="{{route('roles.create')}}" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                     <i class="fas fa-plus"></i>
                    </span>
                                <span class="text">Create Roles</span>
                            </a>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-striped" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Permissions</th>
                                <th>Date Created</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($roles as $role)
                                <tr>
                                    <td>{{$role->name}}</td>
                                    <td>{{$role->description}}</td>
                                    <td> @forelse ($role->permissions()->pluck('name') as $permission)
                                            <span class="badge badge-primary">{{ $permission }}</span>
                                        @empty
                                            <p>No Permission</p>
                                        @endforelse
                                    </td>
                                    <td>{{\Carbon\Carbon::parse($role->created_at)->format('M d Y')}}</td>
                                    <td>
                                        @if($role->name!='administrator')
                                        <a href="{{route('roles.show',$role->id)}}" class="btn btn-success btn-circle">
                                            <i class="fas fa-eye"></i>
                                        </a>

                                        <a href="{{route('roles.edit',$role->id)}}" class="btn btn-info btn-circle">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="#" class="btn btn-danger btn-circle" data-id="{{$role->id}}"   data-toggle="modal" data-target="#deleteModal">
                                            <i class="fas fa-trash-alt"></i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                <td colspan="5">
                                    <h4>No Data</h4>
                                </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        </div>

                        {{ $roles->links( "pagination::bootstrap-4") }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    @include('pages.role._script')
@endsection