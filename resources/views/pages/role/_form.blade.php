@if(isset($role))

    {{ Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'put','id'=>'form','files'=>'true','class'=>'user']) }}
@else
    {{ Form::open(['route' => 'roles.store','id'=>'form','files'=>'true','class'=>'user']) }}
@endif
<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('name') ? ' has-error' : '' }}">
        {{Form::text('name', old('name'),['class' => 'form-control form-control-user','id'=>'name','placeholder'=>'Role Name']) }}
        @if ($errors->has('name'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('description') ? ' has-error' : '' }}">
        {{Form::text('description', old('description'),['class' => 'form-control form-control-user','id'=>'description','placeholder'=>'Description']) }}
        @if ($errors->has('description'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('permissions') ? ' has-error' : '' }}">
        <label for="permissions" class="control-label" aria-describedby="permissions">Permissions</label>
        <small class="form-text text-muted">
            Hold "CTRL" key to select multiple permission
        </small>
        @if(isset($role))
            {!! Form::select('permissions[]', $permissions, old('permissions') ? old('permission') : $role->permissions()->pluck('name', 'name'), ['class' => 'form-control', 'multiple' => 'multiple','id'=>'select-permission',]) !!}
        @else
            {!! Form::select('permissions[]', $permissions, old('permissions'), ['class' => 'form-control', 'multiple' => 'multiple','id'=>'select-permission',]) !!}

        @endif

        @if ($errors->has('permissions'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('permissions') }}</strong>
             </span>
        @endif
    </div>
</div>

@if(isset($role))
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Update Role
    </button>
@else
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Create Role
    </button>
@endif

<hr>

{{ Form::close() }}
