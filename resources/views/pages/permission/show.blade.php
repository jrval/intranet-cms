@extends('layouts.app')
@section('title','Permission'.$permission->name)
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Permissions | {{$permission->name}}</h1>
                </div>
                <div class="col-lg-12">
                    <div class="card shadow mb-4">
{{--                        <div class="card-header py-3">--}}
{{--                            <a href="{{route('permissions.edit',$permission->id)}}" class="btn btn-info btn-icon-split">--}}
{{--                    <span class="icon text-white-50">--}}
{{--                     <i class="fas fa-edit"></i>--}}
{{--                    </span>--}}
{{--                                <span class="text">Edit Permission</span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
                        <div class="card-body">
                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                   <h3>Permission name:</h3>
                                </div>
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    <h3>{{$permission->name}}</h3>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <div class="col-sm-3 mb-3 mb-sm-0">
                                    <h3>Date created:</h3>
                                </div>
                                <div class="col-sm-9 mb-3 mb-sm-0">
                                    <h3>{{\Carbon\Carbon::parse($permission->created_at)->format('M d Y')}}</h3>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection