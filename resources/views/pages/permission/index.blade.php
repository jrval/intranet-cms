@extends('layouts.app')
@section('title','Permission')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Permissions</h1>
                    @if (session('message'))
                        <div class="alert {{session('alert-class')  }}">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                <div class="col-lg-12">
                    <div class="card shadow mb-4">
{{--                        <div class="card-header py-3">--}}
{{--                            <a href="{{route('permissions.create')}}" class="btn btn-success btn-icon-split">--}}
{{--                    <span class="icon text-white-50">--}}
{{--                     <i class="fas fa-plus"></i>--}}
{{--                    </span>--}}
{{--                                <span class="text">Create Permission</span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
                        <div class="table-responsive">
                        <table class="table table-striped"  width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Date Created</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse ($permissions as $permission)
                                <tr>
                                    <td>{{$permission->name}}</td>
                                    <td>{{$permission->description}}</td>
                                    <td>{{\Carbon\Carbon::parse($permission->created_at)->format('M d Y')}}</td>
                                    <td>
{{--                                        <a href="{{route('permissions.show',$permission->id)}}" class="btn btn-success btn-circle">--}}
{{--                                            <i class="fas fa-eye"></i>--}}
{{--                                        </a>--}}
{{--                                        <a href="{{route('permissions.edit',$permission->id)}}" class="btn btn-info btn-circle">--}}
{{--                                            <i class="fas fa-edit"></i>--}}
{{--                                        </a>--}}
{{--                                        <a href="#" class="btn btn-danger btn-circle" data-id="{{$permission->id}}"   data-toggle="modal" data-target="#deleteModal">--}}
{{--                                            <i class="fas fa-trash-alt"></i>--}}
{{--                                        </a>--}}

                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="4">

                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        </div>

                        {{ $permissions->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    @include('pages.permission._script')
@endsection