<script>
    /*render delete modal*/
    $('#deleteModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var permission_id = button.data('id');// Extract info from data-* attributes

        var modal = $(this);

        var url = '{{ route("permissions.destroy", ":permission_id") }}';
        url = url.replace(':permission_id', permission_id);

        modal.find('#delete-form').attr('action', url);
        modal.find('.modal-title').html('DELETE PERMISSION');
        modal.find('.modal-body').html('Are you sure you want to delete this permission?');
    });
</script>