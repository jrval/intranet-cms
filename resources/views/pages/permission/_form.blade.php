@if(isset($permission))

    {{ Form::model($permission, ['route' => ['permissions.update', $permission->id], 'method' => 'put','id'=>'form','files'=>'true','class'=>'user']) }}
@else
    {{ Form::open(['route' => 'permissions.store','id'=>'form','files'=>'true','class'=>'user']) }}
@endif
<div class="form-group row">
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('name') ? ' has-error' : '' }}">
        {{Form::text('name', old('name'),['class' => 'form-control form-control-user','id'=>'name','placeholder'=>'Permission Name']) }}
        @if ($errors->has('name'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
    <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('description') ? ' has-error' : '' }}">
        {{Form::text('description', old('description'),['class' => 'form-control form-control-user','id'=>'description','placeholder'=>'Description']) }}
        @if ($errors->has('description'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
    </div>
</div>

@if(isset($permission))
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Update Permission
    </button>
@else
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Create Permission
    </button>
@endif

<hr>

{{ Form::close() }}
