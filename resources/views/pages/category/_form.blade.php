@if(isset($category))

    {{ Form::model($category, ['route' => ['categories.update', $category->id], 'method' => 'put','id'=>'form','files'=>'true','class'=>'user']) }}
@else
    {{ Form::open(['route' => 'categories.store','id'=>'form','files'=>'true','class'=>'user']) }}
@endif
<div class="form-group row">
    <div class="col-sm-12 mb-3 mb-sm-0 {{ $errors->has('category_name') ? ' has-error' : '' }}">
        {{Form::text('category_name', old('category_name'),['class' => 'form-control form-control-user','id'=>'name','placeholder'=>'Category Name']) }}
        @if ($errors->has('category_name'))
            <span class="help-block" style="color: #a94442;">
                <strong>{{ $errors->first('category_name') }}</strong>
            </span>
        @endif
    </div>
</div>

@if(isset($category))
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Update Category
    </button>
@else
    <button type="submit" class="btn btn-primary btn-user btn-block">
        Create Category
    </button>
@endif

<hr>

{{ Form::close() }}
