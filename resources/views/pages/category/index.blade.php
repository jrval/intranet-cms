@extends('layouts.app')
@section('title','Categories')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Categories</h1>
                    @if (session('message'))
                        <div class="alert {{session('alert-class')  }}">
                            {{ session('message') }}
                        </div>
                    @endif
                </div>
                <div class="col-lg-12">
                    <div class="card shadow mb-4">
                                                <div class="card-header py-3">
                                                    <a href="{{route('categories.create')}}" class="btn btn-success btn-icon-split">
                                            <span class="icon text-white-50">
                                             <i class="fas fa-plus"></i>
                                            </span>
                                                        <span class="text">Create Category</span>
                                                    </a>
                                                </div>
                        <div class="table-responsive">
                            <table class="table table-striped"  width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Date Created</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($categories as $category)
                                    <tr>
                                        <td>{{$category->category_name}}</td>
                                        <td>{{\Carbon\Carbon::parse($category->created_at)->format('M d Y')}}</td>
                                        <td>
                                            <a href="{{route('categories.show',$category->id)}}" class="btn btn-success btn-circle">
                                                <i class="fas fa-eye"></i>
                                            </a>
                                            <a href="{{route('categories.edit',$category->id)}}" class="btn btn-info btn-circle">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                            <a href="#" class="btn btn-danger btn-circle" data-id="{{$category->id}}"   data-toggle="modal" data-target="#deleteModal">
                                                <i class="fas fa-trash-alt"></i>
                                            </a>

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3">
                                            <p class="text-center">No Data</p>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>

                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_js')
    @include('pages.category._script')
@endsection