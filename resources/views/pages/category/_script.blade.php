<script>
    /*render delete modal*/
    $('#deleteModal').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget); // Button that triggered the modal
        var category_id = button.data('id');// Extract info from data-* attributes

        var modal = $(this);

        var url = '{{ route("categories.destroy", ":category_id") }}';
        url = url.replace(':category_id', category_id);

        modal.find('#delete-form').attr('action', url);
        modal.find('.modal-title').html('DELETE CATEGORY');
        modal.find('.modal-body').html('Are you sure you want to delete this category?');
    });
</script>