<!-- Nav Item - Dashboard -->
<li class="nav-item {{ Request::segment(1) == 'home'  ? 'active' : ''}} {{Request::segment(1) == ''  ? 'active' : '' }}">
    <a class="nav-link" href="{{route('home')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>

@can('manage_users')
<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    USER MANAGEMENT
</div>

<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ Request::segment(1) == 'permissions'  ? 'active' : ''}}">
    <a class="nav-link" href="{{route('permissions.index')}}">
        <i class="fas fa-user-shield"></i>
        <span>Permissions</span></a>
</li>
<li class="nav-item {{ Request::segment(1) == 'roles'  ? 'active' : ''}}">
    <a class="nav-link" href="{{route('roles.index')}}">
        <i class="fas fa-user-tag"></i>
        <span>Roles</span></a>
</li>
<li class="nav-item {{ Request::segment(1) == 'users'  ? 'active' : ''}}">
    <a class="nav-link" href="{{route('users.index')}}">
        <i class="fas fa-users"></i>
        <span>Users</span></a>
</li>
@endcan

@can('manage_content')

<!-- Divider -->
<hr class="sidebar-divider">

<!-- Heading -->
<div class="sidebar-heading">
    Intranet
</div>
<!-- Nav Item - Pages Collapse Menu -->
<li class="nav-item {{ Request::segment(1) == 'categories'  ? 'active' : ''}}">
    <a class="nav-link" href="{{route('categories.index')}}">
        <i class="fas fa-list"></i>
        <span>Manage Categories</span></a>
</li>

<li class="nav-item {{ Request::segment(1) == 'posts'  ? 'active' : ''}}">
    <a class="nav-link" href="{{route('posts.index')}}">
        <i class="far fa-clipboard"></i>
        <span>Posts</span></a>
</li>

@endcan
