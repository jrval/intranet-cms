
<!-- Sidebar -->
<ul class="navbar-nav  sidebar sidebar-dark accordion " id="accordionSidebar" style="background-color:#880000">
    <br>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('home')}}">
        {{--        <div class="sidebar-brand-icon rotate-n-15">--}}
        {{--            <i class="fas fa-laugh-wink"></i>--}}
        {{--        </div>--}}
        {{--        <div class="sidebar-brand-text mx-3">VMC Admin <sup>SAP PR-PO</sup></div>--}}

        <div class="mx-3">

            <img class="img-fluid " width="80" src="{{asset('images/logo/vcylogo.png')}}">

        </div>

    </a>
    <br>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

{{--NAVBAR-ITEMS--}}
@include('layouts.sidebar._nav-item')
{{--END NAVBAR ITEMS--}}
<!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
