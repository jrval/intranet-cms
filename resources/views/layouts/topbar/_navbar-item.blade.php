<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">
    <div class="topbar-divider d-none d-sm-block"></div>

    <!-- Nav Item - User Information -->
    <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            @guest
            <span class="mr-2 d-none d-lg-inline text-gray-600 small">guest
            </span>
            @else
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{auth()->user()->name}}
            </span>
            @endguest
            <img class="img-profile rounded-circle" src="{{asset('images/logo/vcylogo.png')}}">
        </a>
        <!-- Dropdown - User Information -->
        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
            <a class="dropdown-item" href="{{route('profile')}}">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Profile
            </a>
{{--            <a class="dropdown-item" href="#">--}}
{{--                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                Settings--}}
{{--            </a>--}}
{{--            <a class="dropdown-item" href="{{route('activity.log')}}">--}}
{{--                <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                Activity Logs--}}
{{--            </a>--}}
{{--            <a class="dropdown-item" href="{{route('log-viewer::dashboard')}}">--}}
{{--                <i class="fas fa-outdent fa-sm fa-fw mr-2 text-gray-400"></i>--}}
{{--                System Logs--}}
{{--            </a>--}}
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Logout
            </a>
        </div>
    </li>

</ul>