@extends('layouts.app')
@section('title','Home')
@section('content')

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            {{--        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>--}}
        </div>

        <div class="jumbotron">
            <h1 class="display-4">Hello, {{auth()->user()->name}}</h1>
            <p class="lead">This is the Content Management System of VCY Intranet</p>
            <hr class="my-4">
            <p>You can visit the intranet here</p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="#" role="button">Intranet</a>
            </p>
        </div>

    </div>
    <!-- /.container-fluid -->
@endsection
@section('page_js')
    @include('_script')
@endsection