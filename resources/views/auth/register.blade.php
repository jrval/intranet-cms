
@extends('layouts.app')

@section('content')
    <div class="row">
        {{--    <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>--}}
        <div class="col-lg-12">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                </div>
                <form class="user" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group row ">
                        <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('name') ? ' has-error' : '' }}">
                            <input name="name" value="{{ old('name') }}" type="text" class="form-control form-control-user" placeholder="Full Name">

                            @if ($errors->has('name'))
                                <span class="help-block" style="color: #a94442;">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('username') ? ' has-error' : '' }}">
                            <input name="username" value="{{ old('username') }}" type="text" class="form-control form-control-user" placeholder="Username">

                            @if ($errors->has('username'))
                                <span class="help-block" style="color: #a94442;">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0 {{ $errors->has('password') ? ' has-error' : '' }}">
                            <input name="password" type="password" class="form-control form-control-user" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="help-block" style="color: #a94442;">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="col-sm-6 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input name="password_confirmation" type="password" class="form-control form-control-user"  placeholder="Confirm password">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block" style="color: #a94442;">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                        Register Account
                    </button>
                    <hr>
                </form>

                <div class="text-center">
                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                </div>
                <div class="text-center">
                    <a class="small" href="login.html">Already have an account? Login!</a>
                </div>
            </div>
        </div>
    </div>
@endsection