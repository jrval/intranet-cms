<?php
/**
 * Copyright (c) 2019. JR Valencia (Developer).
 * Email: valencia_jr08@live.com
 * Redistribution and use in source and binary forms are permitted provided that the above copyright notice and this paragraph are duplicated in all such forms and that any documentation, advertising materials, and other materials related to such distribution and use acknowledge that the software was developed by me.
 *
 * I may not be used to endorse or promote products derived from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED "AS IN" AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE .
 */


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();



Route::group(['middleware' => 'auth'], function() {

    //HOME
    Route::get('/', function(){
        return redirect('home');
    })->name('home');


    Route::get('home', 'HomeController@index')->name('home');

    //PROFILE

    Route::get('profile', 'UserController@profile')->name('profile');
    Route::put('profile', 'UserController@profileUpdate')->name('profile.update');


//  MANAGE USERS
    Route::group(['middleware' => ['permission:manage_users']], function () {

        Route::get('register', function(){
            return redirect('home');
        })->name('register');

        /*PERMISSIONS*/
        Route::resource('permissions', 'PermissionController', ['only' => [
            'index','destroy'
        ]]);

        /*ROLES*/
        Route::resource('roles', 'RoleController');

        /*USERS*/
        Route::resource('users', 'UserController');
        Route::any('users/users_data', 'UserController@usersDatatable')->name('users.dataTable');




    });

    //MANAGE INTRANET

    Route::group(['middleware' => ['permission:manage_content']], function () {


        /*CATEGORIES*/
        Route::resource('categories', 'CategoryController');

        /*POSTS*/
        Route::resource('posts', 'PostController');
        Route::any('posts_data', 'PostController@postDatatable')->name('post.dataTable');

    });


});


